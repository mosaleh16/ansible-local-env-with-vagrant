The goal of this repo is to create a local environment to work with Ansible. The environment consist of 1 Controller/Master node and 2 worker/target nodes. It is created using Vagrant and VirtualBox.


## Prerequisites

The following should be installed in your system:
    - VirtualBox
    - Vagrant

## Getting Started

```shell
vagrant up
```


```shell
./test/test.sh
```

To connect to the controller node, run the following:

```shell
vagrant ssh acontroller
```

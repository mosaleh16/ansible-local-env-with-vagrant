#!/bin/bash

echo "[TASK 1] Install Ansible"
apt-get update -qq -y
apt-get install -qq -y ansible sshpass
ansible --version

echo "[TASK 2] Enable ssh password authentication"
sed -i 's/^PasswordAuthentication .*/PasswordAuthentication yes/' /etc/ssh/sshd_config
echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config
systemctl reload sshd >/dev/null 2>&1

echo "[TASK 3] Set root password"
echo -e "ansibleadmin\nansibleadmin" | passwd root
echo -e "ansibleadmin\nansibleadmin" | passwd vagrant
echo "export TERM=xterm" >> /etc/bash.bashrc >/dev/null 2>&1

echo "[TASK 4] Update /etc/hosts file"
cat >>/etc/hosts<<EOF
192.168.56.10   acontroller.example.com acontroller
192.168.56.11   aworker1.example.com    aworker1
192.168.56.12   aworker2.example.com    aworker2
EOF

echo "[TASK 5] Disable SSH key host checking"
sed -i -e 's/#host_key_checking/host_key_checking/g' /etc/ansible/ansible.cfg >/dev/null 2>&1

ENV['VAGRANT_NO_PARALLEL'] = 'no'

VAGRANT_BOX         = "ubuntu/focal64"
VAGRANT_BOX_VERSION = ""
CPUS_MASTER_NODE    = 1
CPUS_WORKER_NODE    = 1
MEMORY_MASTER_NODE  = 1024
MEMORY_WORKER_NODE  = 1024
WORKER_NODES_COUNT  = 2


Vagrant.configure(2) do |config|

  config.vm.provision "shell", path: "bootstrap.sh"

  # Ansible Controller Machine
  config.vm.define "acontroller" do |node|

    node.vm.box               = VAGRANT_BOX
    node.vm.box_check_update  = false
    node.vm.box_version       = VAGRANT_BOX_VERSION
    node.vm.hostname          = "acontroller"

    node.vm.network "private_network", ip: "192.168.56.10"

    node.vm.provider :virtualbox do |v|
      v.name    = "acontroller"
      v.memory  = MEMORY_MASTER_NODE
      v.cpus    = CPUS_MASTER_NODE
    end

    node.vm.provider :libvirt do |v|
      v.memory  = MEMORY_MASTER_NODE
      v.nested  = true
      v.cpus    = CPUS_MASTER_NODE
    end
  end


  # Ansible Worker Nodes
  (1..WORKER_NODES_COUNT).each do |i|

    config.vm.define "aworker#{i}" do |node|

      node.vm.box               = VAGRANT_BOX
      node.vm.box_check_update  = false
      node.vm.box_version       = VAGRANT_BOX_VERSION
      node.vm.hostname          = "aworker#{i}.example.com"

      node.vm.network "private_network", ip: "192.168.56.1#{i}"

      node.vm.provider :virtualbox do |v|
        v.name    = "aworker#{i}"
        v.memory  = MEMORY_WORKER_NODE
        v.cpus    = CPUS_WORKER_NODE
      end

      node.vm.provider :libvirt do |v|
        v.memory  = MEMORY_WORKER_NODE
        v.nested  = true
        v.cpus    = CPUS_WORKER_NODE
      end
    end
  end
end
